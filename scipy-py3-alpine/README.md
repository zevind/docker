# Scipy/Numpy minimal Python 3 Alpine image

Optimized for size on Alpine.  This image is just over 210MB and has:

* Alpine 3.10
* Python 3.7.3
* Numpy 1.17.1
* Scipy 1.3.1
